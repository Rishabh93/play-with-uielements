//
//  ViewController.h
//  ButtonHide
//
//  Created by Rishabh  on 11/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic) NSUInteger numberOfTapsRequired;
@property (nonatomic) UILabel *myLabel;
@property (nonatomic) UIButton *myButtonForLongPress;
@property (nonatomic) UIButton *myButtonForText;
@property (nonatomic) UIImageView *myImage;
@property (nonatomic) UITextField *myTextField;
@property (nonatomic) UISegmentedControl *mySegmentControl;
@property (nonatomic) UILabel *myLabelForSlider;
@property (nonatomic) UIActivityIndicatorView *myActivityIndicator;
@property (nonatomic) UIProgressView *myProgressBar;
@property (nonatomic) UIButton *myBackButton;
//-(void) hideActivityIndicator;

@end

