//
//  ViewController.m
//  ButtonHide
//
//  Created by Rishabh  on 11/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate,UITextFieldDelegate,UIAlertViewDelegate>
@property UITapGestureRecognizer *gesture;


@end

@implementation ViewController
CGFloat lastScale,lastPoint;
CGPoint initialPossitionOfLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myButtonForText = [[UIButton alloc] initWithFrame:CGRectMake(150, 300, 100, 50)];
    self.myButtonForText.backgroundColor = [UIColor greenColor];
    [self.myButtonForText setTitle:@"Main Button" forState:UIControlStateNormal];
    [self.myButtonForText setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:self.myButtonForText];
    [self.myButtonForText addTarget:self action:@selector(myMainAction:) forControlEvents:UIControlEventTouchUpInside];
  
}


-(void) myMainAction : (UIButton *) sender {
    sender.hidden = YES;
//    self.myBackButton.hidden = NO;
    NSInteger randomNumber = arc4random_uniform(9);
    NSLog(@"%d",randomNumber);
    switch (randomNumber) {
        case 0:
        {
            CGRect labelLocation = CGRectMake(150, 300, 200, 50);
            self.myLabel = [[UILabel alloc] initWithFrame:labelLocation];
            self.myLabel.text = @"RISHABH GUPTA";
            self.myLabel.textColor = [UIColor blackColor];
            self.myLabel.font = [UIFont boldSystemFontOfSize:20];
            //label.font = [UIFont italicSystemFontOfSize:30];
            //label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
            self.myLabel.backgroundColor = [UIColor redColor];
            //    label.textAlignment = UITextAlignmentCenter;
            self.myLabel.textAlignment = NSTextAlignmentCenter;
            
            self.myLabel.userInteractionEnabled = YES;
            initialPossitionOfLabel = self.myLabel.center;
            UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(labelDragged:)];
            [self.myLabel addGestureRecognizer:gesture];
            [self.view addSubview:self.myLabel];
            //            [self.myLabel setHidden:YES];
            
        }
            break;
            
        case 1:
        {
            CGRect buttonLocation = CGRectMake(150, 300, 200, 50);
            self.myButtonForLongPress = [[UIButton alloc]initWithFrame:buttonLocation];
            self.myButtonForLongPress.backgroundColor = [UIColor greenColor];
            [self.myButtonForLongPress setTitle:@"Click Me" forState:UIControlStateNormal];
            [self.myButtonForLongPress setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            [self.view addSubview:self.myButtonForLongPress];
            
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
            [self.myButtonForLongPress addGestureRecognizer:longPress];
            
        }
            break;
            
        case 2:
        {
            CGRect imageLocation = CGRectMake(150, 300, 100, 100);
            self.myImage = [[UIImageView alloc]initWithFrame:imageLocation];
            self.myImage.image = [UIImage imageNamed:@"Bananaimage .png"];
            self.myImage.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapResponder:)];
            tapRecognizer.numberOfTapsRequired = 2;
            //    tapRecognizer.numberOfTouchesRequired = 1;
            [self.view addSubview:self.myImage];
            [self.myImage addGestureRecognizer:tapRecognizer];
            UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapResponder:)];
            tapRecognizer2.numberOfTapsRequired = 1;
            //    tapRecognizer.numberOfTouchesRequired = 1;
            [self.view addSubview:self.myImage];
            [self.myImage addGestureRecognizer:tapRecognizer2];
//            [self.myImage setHidden:YES];
            
        }
            break;
        case 3:
        {
            self.view.backgroundColor = [UIColor yellowColor];
            UIPinchGestureRecognizer *twoFingerPinch = [[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(pinchMethod:)];
            [self.view addGestureRecognizer:twoFingerPinch];
            
        }
            break;
            
        case 4:
        {
            NSArray *itemArray = [NSArray arrayWithObjects: @"One", @"Two", @"Three",nil];
            self.mySegmentControl = [[UISegmentedControl alloc] initWithItems:itemArray];
            self.mySegmentControl .frame = CGRectMake(150, 300, 250, 50);
            self.mySegmentControl .segmentedControlStyle = UISegmentedControlStylePlain;
            [self.mySegmentControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents: UIControlEventValueChanged];
            self.mySegmentControl .selectedSegmentIndex = 1;
            [self.view addSubview:self.mySegmentControl ];
//            self.mySegmentControl.hidden = YES;
            
        }
            break;
        case 5:
        {
            self.myActivityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, 300, 50, 50)];
            self.myActivityIndicator.backgroundColor = [UIColor redColor];
            [self.view addSubview:self.myActivityIndicator];
            [self.myActivityIndicator startAnimating];
            //    [self hideActivityIndicator];
        }
            break;
        case 6:
        {
            UISwitch *switch1 = [[UISwitch alloc]initWithFrame:CGRectMake(150, 300, 200, 50)];
            [switch1 addTarget:self action:@selector(mySwitch:) forControlEvents:UIControlEventValueChanged];
            [self.view addSubview:switch1];
//            switch1.hidden = YES;
        }
            break;
        case 7:
        {
            self.myTextField = [[UITextField alloc] initWithFrame:CGRectMake(150, 300, 100, 50)];
            self.myTextField.text = @"Rishabh";
            self.myTextField.font =[UIFont fontWithName:@"itelic" size:20];
            self.myTextField.backgroundColor = [UIColor blueColor];
            self.myTextField.textAlignment = NSTextAlignmentCenter;
            [self.view addSubview:self.myTextField];
            self.myTextField.delegate = self;
//            self.myTextField.hidden = YES;
        }
            break;
        case 8:
        {
            UISlider *slider = [[UISlider alloc]initWithFrame:CGRectMake(150, 300, 100, 50)];
            slider.backgroundColor = [UIColor blueColor];
            [self.view addSubview:slider];
            slider.minimumValue = 1;
            slider.maximumValue = 100;
            [slider addTarget:self action:@selector(mySliderFunction:) forControlEvents:UIControlEventValueChanged];
            self.myLabelForSlider = [[UILabel alloc] initWithFrame:CGRectMake(150, 200, 80, 50)];
            self.myLabelForSlider.backgroundColor = [UIColor redColor];
            [self.view addSubview:self.myLabelForSlider];
            self.myProgressBar = [[UIProgressView alloc] initWithFrame:CGRectMake(150, 450, 200, 1000)];
            [self.view addSubview:self.myProgressBar];
//          slider.hidden =YES;
//          self.myLabelForSlider.hidden = YES;
            
        }
        
            
    }
    
}

- (void)labelDragged:(UIPanGestureRecognizer *)gesture
{
    UILabel *label = (UILabel *)gesture.view;
    CGPoint translation = [gesture translationInView:label];
    
    // move label
    label.center = CGPointMake(label.center.x + translation.x,
                               label.center.y + translation.y);
    
    // reset translation
    [gesture setTranslation:CGPointZero inView:label];
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        label.center = initialPossitionOfLabel;
    }
}

- (void)longPress:(UILongPressGestureRecognizer*)gesture {
    
    if ( gesture.state == UIGestureRecognizerStateEnded ) {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"You Pressed!!" message:@"Long Press !!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void) tapResponder : (UITapGestureRecognizer *) sender {
    
    if(sender.numberOfTapsRequired == 1) {
        NSLog(@"Single Tap detected");
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"You Tapped the image!!" message:@"One Time !!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    } else if(sender.numberOfTapsRequired == 2) {
        NSLog(@"Double Tap detected");
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"You Tapped the image!!" message:@"Two Times !!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}



- (void)MySegmentControlAction:(UISegmentedControl *)segment
{
    if(segment.selectedSegmentIndex == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Segment Selected !!" message:@"First Segment Selected !!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else if(segment.selectedSegmentIndex == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Segment Selected !!" message:@"Second Segment Selected !!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Segment Selected !!" message:@"Third Segment Selected !!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void) mySwitch : (UISwitch *) sender {
    if([sender isOn]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Switch Selected !!" message:@"Switch ON  !!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", @"Done", nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Switch Selected !!" message:@"Switch OFF  !!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", @"Done", nil];
        [alert show];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *str = self.myTextField.text;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TextField Return !!" message:str delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    [self.myTextField resignFirstResponder ];
    return YES;
}

- (void)pinchMethod:(UIPinchGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGFloat scale = [recognizer scale];
        [recognizer.view setTransform:CGAffineTransformScale(recognizer.view.transform, scale, scale)];
        [recognizer setScale:1.0];
    }
}

-(void) mySliderFunction : (UISlider *) sender {
    NSString *value = [NSString stringWithFormat:@"%f", sender.value];
    self.myLabelForSlider.text = value;
    self.myProgressBar.progress = sender.value/100;
}

- (void)alertView:(UIAlertView *)theAlertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSLog(@"You Pressed OK  !!");
    }
    else if (buttonIndex == 2) {
        NSLog(@"You Pressed DONE  !!");
    }
    else if (buttonIndex == 0) {
        NSLog(@"You Pressed CANCEL  !!");
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
